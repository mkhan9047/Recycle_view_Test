package bd.pro.coint.recyclerview;

import android.annotation.SuppressLint;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Mujahid on 7/26/2018.
 */

public class FoodCustomAdapter extends RecyclerView.Adapter<FoodCustomAdapter.FoodViewHolder> {

    ArrayList<FoodMenu> foodList;


    public FoodCustomAdapter(ArrayList<FoodMenu> foodList) {
        this.foodList = foodList;
    }

    @Override
    public FoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View data = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_recycle_custom_view, parent, false);

        return new FoodViewHolder(data);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(FoodViewHolder holder, int position) {
        holder.name.setText(foodList.get(position).getName());
        holder.price.setText(String.format("Price: $%.2f",foodList.get(position).getPrice()));
        holder.image.setImageResource(foodList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }


    class FoodViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView name, price;
        public FoodViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.img);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
        }
    }

}
