package bd.pro.coint.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<FoodMenu> items;
    RecyclerView recyclerView;
    FoodCustomAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyle);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        items = new ArrayList<>();

        FoodMenu cake = new FoodMenu("Cake",5.3, R.drawable.cake);
        FoodMenu burger = new FoodMenu("Burger",9.3, R.drawable.burger);
        FoodMenu coffe = new FoodMenu("Cofee",2.3, R.drawable.coffe);
        FoodMenu drink = new FoodMenu("Drink",1.3, R.drawable.drink);
        FoodMenu fish = new FoodMenu("Fish",15.3, R.drawable.fish);
        FoodMenu fried_rice = new FoodMenu("Fried Rice",3.3, R.drawable.fried_rice);
        FoodMenu layerdCake = new FoodMenu("Layered Cake",7.3, R.drawable.layer_cake);
        FoodMenu pizza = new FoodMenu("Pizza",2.3, R.drawable.pizza);
        FoodMenu soup = new FoodMenu("Soup",2.3, R.drawable.sup);


        items.add(cake);
        items.add(burger);
        items.add(coffe);
        items.add(drink);
        items.add(fish);
        items.add(fried_rice);
        items.add(layerdCake);
        items.add(pizza);
        items.add(soup);




        adapter = new FoodCustomAdapter(items);

        recyclerView.setAdapter(adapter);




    }
}
