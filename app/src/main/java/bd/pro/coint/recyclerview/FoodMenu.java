package bd.pro.coint.recyclerview;

/**
 * Created by Mujahid on 7/26/2018.
 */

public class FoodMenu {
    String name;
    double price;
    int image;

    public FoodMenu(String name, double price, int image) {
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getImage() {
        return image;
    }
}
